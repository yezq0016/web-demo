// import {createChunk} from './createChunk.js'

const CHUNK_SIZE = 1024 * 1024 * 2; // 5MB
const THREAD_COUNT = 4; // 线程数量

// 通过新开线程切片，worker
export function cutFile(file) {
	return new Promise((resolve) => {
		const result = []; // 分片数据
		const chunkCount = Math.ceil(file.size / CHUNK_SIZE); // 分片数量
		const workerChunkCount = Math.ceil(chunkCount / THREAD_COUNT); // 一个线程多少分片
		let finishCount = 0; // 完成分片数
		for(let i=0;i<THREAD_COUNT;i++) {
			// 创建一个新的 Worker 线程
			const worker = new Worker('./worker.js', {
				type: 'module'
			})
			// 计算每个线程的开始索引和结束索引
			const startIndex = i * workerChunkCount;
			let endIndex = startIndex + workerChunkCount;
			if (endIndex > chunkCount) {
				endIndex = chunkCount;
			}
			worker.postMessage({
				file,
				CHUNK_SIZE,
				startIndex,
				endIndex
			})
			worker.onmessage = (e) => {
				// 返回分片数组，因为不同线程返回的消息时间先后，使用循环索引填充分片数据
				for(let j = startIndex; j<endIndex; j++) {
					result[j] = e.data[j-startIndex]
				}
				
				// 结束线程
				worker.terminate();
				finishCount++
				if(finishCount === THREAD_COUNT){
					resolve(result)
				}
			}
		}
	})
	// const chunk = await createChunk(file, 1, CHUNK_SIZE);
}