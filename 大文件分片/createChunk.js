import SparkMD5 from './spark-md5.js'

export function createChunk(file, index, chunkSize) {
	return new Promise((resolve) => {
		const start = index * chunkSize;
		let end = start + chunkSize;
		if(end>file.size) {
			end = file.size
		}
		const spark = new SparkMD5.ArrayBuffer();
		const fileReader = new FileReader();
		fileReader.onload = (e) => {
			spark.append(e.target.result);
			resolve({
				start,
				end,
				index,
				hash: spark.end()
			});
		}
		fileReader.readAsArrayBuffer(file.slice(start, end))
	})
}