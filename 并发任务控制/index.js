class SuperTask {
	constructor(limit = 2) {
		this.limit = limit // 并发数量
		this.runningCount = 0 // 当前运行的任务数
		this.tasks = [] // 任务数组
	}
	// 添加任务
	add(task) {
		return new Promise((resolve, reject)=>{
			// 任务为异步任务，所以把当前的 resolve,reject跟任务一起放到任务数组中，当执行完成任务的时候调用，完成此次promise
			this.tasks.push({
				task,
				resolve,
				reject
			})
			this._run()
		})
	}
	// 执行任务
	_run() {
		while(this.runningCount < this.limit && this.tasks.length){
			const {task,resolve,reject} = this.tasks.shift()
			this.runningCount++
			task().then(resolve, reject).finally(()=>{
				// 任务执行完后，当前执行任务数量减一，并且继续执行任务
				this.runningCount--
				this._run()
			})
		}
	}
}


function timeout(time){
	return new Promise(resolve => {
		setTimeout(()=>{
			resolve()
		}, time)
	})
}

const superTask = new SuperTask()
function addTask(time, name) {
	superTask.add(()=>timeout(time)).then(()=>{
		console.log(`任务${name}完成！`)
	})
}

addTask(10000, 1)
addTask(5000, 2)
addTask(3000, 3)
addTask(4000, 4)
addTask(5000, 5)