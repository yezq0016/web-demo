function _runTask(task, cb) {
	// 方法一
	requestIdleCallback((idle)=>{
		// 判断是否有空闲时间
		if(idle.timeRemaining() > 0) {
			task()
			cb(Date.now())
		}else{
			_runTask(task, cb)
		}
	})
	
	// 方法二，兼容较好
	// let start = Date.now();
	// requestAnimationFrame(()=>{
	// 	if(Date.now() - start < 16.6) {
	// 		task()
	// 		cb(Date.now())
	// 	}else{
	// 		_runTask(task, cb)
	// 	}
	// })
}

/**
 * 运行的耗时任务
 * @param {Function} task
**/
function runTask2(task) {
	return new Promise(resolve=>{
		_runTask(task, resolve)
	})
}

function runTask1(task) {
	task()
}


// 测试
let startTime = Date.now();

function blockingTask() {  
	const txtEle = document.getElementById("timetxt")
	for(let i=0;i<3000;i++) {
		txtEle.innerText = `耗时：${Date.now() - startTime}`
	}
}
// 使用示例，优化前
const onTask1 = async ()=>{
	startTime = Date.now();
	console.log('开始阻塞任务...');
	for(let i=0;i<1000;i++){
		runTask1(blockingTask); // 这将阻塞
	}
	console.log('阻塞任务结束...', Date.now() - startTime);
}
// 使用示例，优化后 
const onTask2 = async ()=>{
	startTime = Date.now();
	console.log('开始阻塞任务...');
	for(let i=0;i<1000;i++){
		runTask2(blockingTask); // 优化，这将阻塞5秒  
	}
	console.log('阻塞任务结束...', Date.now() - startTime);
}